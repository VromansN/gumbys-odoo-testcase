{
    'name': 'Labor Calculations',
    'version': '15.0.0.1',
    'depends': ['base', 'stock', 'sale_management'],
    'category': 'Invoicing',
    'author': 'Nico Vromans',
    'license': 'AGPL-3',
    'description': '''
    Calculate labor and expenses.
    ''',
    # static files (bundles): code (js files), style (css or scss files) and templates (xml files)
    'assets': {

    },
    # data files always loaded at installation (NOTE: the order matters!)
    'data': [
        'security/ir.model.access.csv',
        'views/labor_calculations_views.xml',
        'views/labor_calculations_menus.xml',
    ],
}
