from odoo import fields, models


class InheritedSaleOrderLine(models.Model):
    _name = 'inherited.sale.order.line'
    _inherit = 'sale.order.line'

    order_id = fields.Many2one(
        'labor.calculation',
        string='Order Reference',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )
    invoice_lines = fields.Many2many(
        'account.move.line',
        'inherited_sale_order_line_invoice_rel',
        'inherited_order_line_id',
        'invoice_line_id',
        string='Invoice Lines',
        copy=False
    )
