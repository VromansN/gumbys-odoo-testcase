from odoo import fields, models


class LaborCalculationModel(models.Model):
    _name = 'labor.calculation'
    _description = 'Labor Calculation'
    _inherit = 'sale.order'

    transaction_ids = fields.Many2many(
        'payment.transaction',
        'labor_calculation_transaction_rel',
        'labor_calculation_id',
        'transaction_id',
        string='Transactions', copy=False, readonly=True
    )
    tag_ids = fields.Many2many(
        'crm.tag',
        'labor_calculation_tag_rel',
        'labor_calculation_id',
        'tag_id',
        string='Tags'
    )
    order_line = fields.One2many(
        'inherited.sale.order.line',
        'order_id',
        string='Order Lines',
        states={'cancel': [('readonly', True)], 'done': [('readonly', True)]},
        copy=True,
        auto_join=True)
