# Odoo testcase

## Setup
This setup uses [docker-compose](https://docs.docker.com/compose/).

To run the containers you have two options:

1. using a custom `.env.local` file (safer, because this file is ignored by git):
   1. copy the [.env](.env) file in the root directory and name the copy `.env.local` (also in root directory)
   2. replace the `~` (tildes) with the desired values
   3. run the containers with `docker-compose --env-file .env.local up -d`
2. using the default [.env](.env) file (less safe, make sure to not commit it into git)
   1. replace the `~` (tildes) in the [.env](.env) file with the desired values
   2. run the containers with `docker-compose up -d`

This will spin up 2 containers:
1. [odoo](https://hub.docker.com/_/odoo/) (v15 - Community), reacheble via (choose one):
   1. http://0.0.0.0:8069
   2. http://127.0.0.1:8069
   3. http://localhost:8069
2. [postgres](https://hub.docker.com/_/postgres) (v14)

### First startup
When going to the [front-end](http://localhost:8069) for the first time, you will be promted to login. The following default account will have been automatically created:
- username: `admin`
- password: `admin`

It is highly recommended you change the password immediately after logging in for the first time (or create a new user with the proper access rights, settings, etc., and remove the default admin user altogether).

## Reloading data
Whenever you make any changes to the code, you need to restart the web container: `docker-compose restart web`. This will restart the container, reloading any translations, addons, etc.

## Addon overview
### Activating the addon
1. go to `Menu -> Apps`
2. in the search bar remove the default `Apps` filter, type `Labor Calculations` and hit enter
3. only the `Labor Calculations` app should be visible, click the `Install` button

### Menu
After having installed the addon you can go to the overview from the menu: `Menu -> Labor Calculations`
<!-- TODO: complete this section -->
<!-- TODO: add documentation to enabl Units of Measurement -->
